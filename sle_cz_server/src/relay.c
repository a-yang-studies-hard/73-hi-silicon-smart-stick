#include "../inc/relay.h"
#include "pinctrl.h" // 引脚控制相关的头文件
#include "gpio.h"    // GPIO操作相关的头文件

// 继电器状态变量
static bool relay_state = false;

// 初始化继电器
void relay_init(void)
{
    uapi_pin_set_mode(RELAY_PIN, HAL_PIO_FUNC_GPIO);
    uapi_gpio_set_dir(RELAY_PIN, GPIO_DIRECTION_OUTPUT);
    // 初始化时关闭继电器
    relay_off();
}

// 打开继电器
void relay_on(void)
{
    // 设置继电器引脚为高电平
    uapi_gpio_set_val(RELAY_PIN, GPIO_LEVEL_HIGH);
    relay_state = true;
}

// 关闭继电器
void relay_off(void)
{
    // 设置继电器引脚为低电平
    uapi_gpio_set_val(RELAY_PIN, GPIO_LEVEL_LOW);
    relay_state = false;
}

// 切换继电器状态
void relay_toggle(void)
{
    if (relay_state)
    {
        relay_off();
    }
    else
    {
        relay_on();
    }
}

// 获取继电器状态
bool relay_get_state(void)
{
    return relay_state;
}