
/*
 * Copyright (c) 2024 HiSilicon Technologies CO., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "pinctrl.h"
#include "uart.h"
#include "watchdog.h"
#include "osal_debug.h"
#include "soc_osal.h"
#include "app_init.h"

#define UART_BAUDRATE 4800
#define UART_DATA_BITS 3
#define UART_STOP_BITS 1
#define UART_PARITY_BIT 0
#define UART_TRANSFER_SIZE 8
#define UART_RECEIVE_SIZE 18
#define CONFIG_UART1_TXD_PIN 15
#define CONFIG_UART1_RXD_PIN 16
#define CONFIG_UART1_PIN_MODE 1
#define CONFIG_UART1_BUS_ID 1
#define CONFIG_UART_INT_WAIT_MS 5

#define UART_TASK_STACK_SIZE 0x1000
#define UART_TASK_DURATION_MS 1000
#define UART_TASK_PRIO 17

static uint8_t g_app_uart_tx_buff[UART_TRANSFER_SIZE] = {0x01,0x03,0x00,0x48,0x00,0x08,0xC4,0x1A};
static uint8_t g_app_uart_rx_buff[UART_RECEIVE_SIZE] = {0};

#define CONFIG_UART_POLL_TRANSFER_MODE 1
#if defined(CONFIG_UART_POLL_TRANSFER_MODE)
// 轮询读取uart值
#endif

unsigned long Voltage_data, Current_data, Power_data, Energy_data, Pf_data, CO2_data, HZ;
float Pf_data1, HZ1;
float voltage,current;
void Analysis_data(void)
{
    Voltage_data = (((unsigned long)(g_app_uart_rx_buff[3])) << 24) |
                   (((unsigned long)(g_app_uart_rx_buff[4])) << 16) |
                   (((unsigned long)(g_app_uart_rx_buff[5])) << 8) |
                   g_app_uart_rx_buff[6];
    printf("电压值: "); 
    printf("%d V\n",Voltage_data);
    voltage = (float)Voltage_data;
    // printf("%.4f V\n",Voltage_data/10000.0);

    Current_data = (((unsigned long)(g_app_uart_rx_buff[7])) << 24) |
                   (((unsigned long)(g_app_uart_rx_buff[8])) << 16) |
                   (((unsigned long)(g_app_uart_rx_buff[9])) << 8) |
                   g_app_uart_rx_buff[10];
    printf("电流: ");
    printf("%d A\n",Current_data);
    current = (float)Current_data;
    // printf("%.4f A\n",Current_data/10000.0);

    // Power_data = (((unsigned long)(g_app_uart_rx_buff[11])) << 24) |
    //              (((unsigned long)(g_app_uart_rx_buff[12])) << 16) |
    //              (((unsigned long)(g_app_uart_rx_buff[13])) << 8) |
    //              g_app_uart_rx_buff[14];
    // printf("功率: ");
    // printf("%d\n",Power_data/10000,4);

    // Energy_data = (((unsigned long)(Rx_Buffer1[15])) << 24) |
    //               (((unsigned long)(Rx_Buffer1[16])) << 16) |
    //               (((unsigned long)(Rx_Buffer1[17])) << 8) |
    //               Rx_Buffer1[18];
    // printf("电能: ");
    // printf(Energy_data);

    // Pf_data = (((unsigned long)(Rx_Buffer1[19])) << 24) |
    //            (((unsigned long)(Rx_Buffer1[20])) << 16) |
    //            (((unsigned long)(Rx_Buffer1[21])) << 8) |
    //            Rx_Buffer1[22];
    // Pf_data1 = (float)(Pf_data * 0.001);
    // Serial.print("功率因数: ");
    // Serial.println(Pf_data1, 3);

    // CO2_data = (((unsigned long)(Rx_Buffer1[23])) << 24) |
    //             (((unsigned long)(Rx_Buffer1[24])) << 16) |
    //             (((unsigned long)(Rx_Buffer1[25])) << 8) |
    //             Rx_Buffer1[26];
    // Serial.print("二氧化碳: ");
    // Serial.println(CO2_data);

    // HZ = (((unsigned long)(Rx_Buffer1[31])) << 24) |
    //      (((unsigned long)(Rx_Buffer1[32])) << 16) |
    //      (((unsigned long)(Rx_Buffer1[33])) << 8) |
    //      Rx_Buffer1[34];
    // HZ1 = HZ * 0.01;
    // Serial.print("赫兹: ");
    // Serial.println(HZ1, 2);
}


static uart_buffer_config_t g_app_uart_buffer_config = {.rx_buffer = g_app_uart_rx_buff,
                                                        .rx_buffer_size = UART_TRANSFER_SIZE};

static void app_uart_init_pin(void)
{
    uapi_pin_set_mode(CONFIG_UART1_TXD_PIN, CONFIG_UART1_PIN_MODE);
    uapi_pin_set_mode(CONFIG_UART1_RXD_PIN, CONFIG_UART1_PIN_MODE);
}

static void app_uart_init_config(void)
{
    uart_attr_t attr = {.baud_rate = UART_BAUDRATE,
                        .data_bits = UART_DATA_BITS,
                        .stop_bits = UART_STOP_BITS,
                        .parity = UART_PARITY_BIT};

    uart_pin_config_t pin_config = {.tx_pin = S_MGPIO0, .rx_pin = S_MGPIO1, .cts_pin = PIN_NONE, .rts_pin = PIN_NONE};
    uapi_uart_deinit(CONFIG_UART1_BUS_ID); // 重点，UART初始化之前需要去初始化，否则会报0x80001044
    int res = uapi_uart_init(CONFIG_UART1_BUS_ID, &pin_config, &attr, NULL, &g_app_uart_buffer_config);
    if (res != 0) {
        printf("uart init failed res = %02x\r\n", res);
    }
}


void *uart_task(const char *arg)
{
    unused(arg);
    /* UART pinmux. */
    app_uart_init_pin();
    /* UART init config. */
    app_uart_init_config();

    while (1)
    {
        osal_mdelay(UART_TASK_DURATION_MS);

        osal_printk("uart%d poll mode send start!, len = %d\r\n", CONFIG_UART1_BUS_ID, sizeof(g_app_uart_rx_buff));
        (void)uapi_watchdog_kick();
        if (uapi_uart_write(CONFIG_UART1_BUS_ID, g_app_uart_tx_buff, UART_TRANSFER_SIZE, 0) == UART_TRANSFER_SIZE) {
            osal_printk("uart%d poll mode send back succ!\r\n", CONFIG_UART1_BUS_ID);
        }
        if (uapi_uart_read(CONFIG_UART1_BUS_ID, g_app_uart_rx_buff, 18, 0) > 0) {
            osal_printk("uart%d poll mode receive succ!, g_app_uart_rx_buff = %s\r\n", CONFIG_UART1_BUS_ID,
                        g_app_uart_rx_buff);
        }
        // printf("g_app_uart_rx_buff = \n");

        // for (int i = 0; i < UART_RECEIVE_SIZE; i++)
        // {
        //     printf(" %u",g_app_uart_rx_buff[i]);
        //     printf(" ");

        // }
        // printf(" \n");
        Analysis_data();
        osal_msleep(800);
    }

    return NULL;
}

static void uart_entry(void)
{
    osal_task *task_handle = NULL;
    osal_kthread_lock();
    task_handle = osal_kthread_create((osal_kthread_handler)uart_task, 0, "UartTask", UART_TASK_STACK_SIZE);
    if (task_handle != NULL) {
        osal_kthread_set_priority(task_handle, UART_TASK_PRIO);
        osal_kfree(task_handle);
    }
    osal_kthread_unlock();
}

/* Run the uart_entry. */
app_run(uart_entry);


