#ifndef RELAY_H
#define RELAY_H

#include <stdint.h>
#include <stdbool.h>

// 定义继电器的引脚
#define RELAY_PIN 1

// 初始化继电器
void relay_init(void);

// 打开继电器
void relay_on(void);

// 关闭继电器
void relay_off(void);

// 切换继电器状态
void relay_toggle(void);

// 获取继电器状态
bool relay_get_state(void);

#endif // RELAY_H