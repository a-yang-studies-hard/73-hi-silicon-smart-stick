#ifndef __MPU6050_H__
#define __MPU6050_H__

#define	MPU6050_SMPLRT_DIV		0x19
#define	MPU6050_CONFIG			0x1A
#define	MPU6050_GYRO_CONFIG		0x1B
#define	MPU6050_ACCEL_CONFIG	0x1C

#define	MPU6050_ACCEL_XOUT_H	0x3B
#define	MPU6050_ACCEL_XOUT_L	0x3C
#define	MPU6050_ACCEL_YOUT_H	0x3D
#define	MPU6050_ACCEL_YOUT_L	0x3E
#define	MPU6050_ACCEL_ZOUT_H	0x3F
#define	MPU6050_ACCEL_ZOUT_L	0x40
#define	MPU6050_TEMP_OUT_H		0x41
#define	MPU6050_TEMP_OUT_L		0x42
#define	MPU6050_GYRO_XOUT_H		0x43
#define	MPU6050_GYRO_XOUT_L		0x44
#define	MPU6050_GYRO_YOUT_H		0x45
#define	MPU6050_GYRO_YOUT_L		0x46
#define	MPU6050_GYRO_ZOUT_H		0x47
#define	MPU6050_GYRO_ZOUT_L		0x48

#define	MPU6050_PWR_MGMT_1		0x6B
#define	MPU6050_PWR_MGMT_2		0x6C
#define	MPU6050_WHO_AM_I		0x75
// ***********************************************
#define MPU6050_ADDRESS_AD0_LOW     0x68  // address pin low (GND), default for InvenSense evaluation board
#define MPU6050_RA_CONFIG           0x1A
#define MPU6050_RA_ACCEL_CONFIG     0x1C
#define MPU6050_RA_FF_THR           0x1D
#define MPU6050_RA_FF_DUR           0x1E
#define MPU6050_RA_MOT_THR          0x1F  // 运动检测阀值设置寄存器
#define MPU6050_RA_MOT_DUR          0x20  // 运动检测时间阀值
#define MPU6050_RA_ZRMOT_THR        0x21
#define MPU6050_RA_ZRMOT_DUR        0x22
#define MPU6050_RA_FIFO_EN          0x23
#define MPU6050_RA_INT_PIN_CFG      0x37   // 中断/旁路设置寄存器
#define MPU6050_RA_INT_ENABLE       0x38   // 中断使能寄存器
#define MPU6050_RA_TEMP_OUT_H       0x41
#define MPU6050_RA_USER_CTRL        0x6A
#define MPU6050_RA_PWR_MGMT_1       0x6B
#define MPU6050_RA_WHO_AM_I         0x75


// #define MPU6050_ADDRESS		    0xD0		//MPU6050的I2C从机地址
#define MPU6050_ADDRESS         0x68  // MPU6050器件读地址
#define CONFIG_I2C_MASTER_BUS_ID 1

#define I2C_MASTER_ADDR                   0x0
#define I2C_SLAVE_ADDR                    MPU6050_ADDRESS
#define I2C_SET_BAUDRATE                  400000 // I2C 波特率
#define I2C_TASK_DURATION_MS              10
#define CONFIG_I2C_SCL_MASTER_PIN 15
#define CONFIG_I2C_SDA_MASTER_PIN 16
#define CONFIG_I2C_MASTER_PIN_MODE 2
#if defined(CONFIG_I2C_SUPPORT_INT) && (CONFIG_I2C_SUPPORT_INT == 1)
#define I2C_INT_TRANSFER_DELAY_MS         800
#endif

#define BUTTON_GPIO 13 // 按键
//卡尔曼滤波算法结构体
#include <math.h>  
  
typedef struct {  
    float Q; // Process noise covariance
    float R; // Measurement noise covariance
    float P; // Error covariance estimate
    float K; // Kalman gain
    float X; // State estimate
} kalman_filter_t;


// 用于存储角度的结构体
typedef struct {
    float pitch; // 绕X轴旋转的角度
    float roll;  // 绕Y轴旋转的角度
    float yaw;   // 绕Z轴旋转的角度
} angle_t;

angle_t current_angle = {0.0f, 0.0f, 0.0f}; // 初始化角度为零
errcode_t ret;
uint32_t baudrate = I2C_SET_BAUDRATE;
uint8_t hscode = I2C_MASTER_ADDR;

int16_t AccX, AccY, AccZ, GyroX, GyroY, GyroZ;

#endif