#include "pinctrl.h"
#include "i2c.h"
#include "mpu6050.h"
#include "soc_osal.h"
#include "app_init.h"
#include <stdint.h>
#include "gpio.h"
#include <stdio.h>
#include <stdlib.h> // 包含stdlib.h以使用malloc和free

static int shanghui, xiahui, zuoxuan, youxuan = 0; // 基础手势动作变量

static int Gesture_flag = 0; // 手势识别变量
typedef struct
{
    float acc_x, acc_y, acc_z;
    float gyro_x, gyro_y, gyro_z;
} SensorData;

// 初始化标准手势
SensorData standardMotion1 = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
SensorData Motion1 = {
    // 迪迦变身
    -0.09f, // acc_x
    0.04f,  // acc_y
    -0.09f, // acc_z
    18.91f, // gyro_x
    49.59f, // gyro_y
    -24.56f // gyro_z
};
SensorData Motion2 = {
    // L动作
    0.01f,  // acc_x
    0.03f,  // acc_y
    0.13f,  // acc_z
    -1.49f, // gyro_x
    -6.54f, // gyro_y
    10.01f  // gyro_z
};

SensorData Motion3 = {
    // 竖直向下动作
    -0.00f,  // acc_x
    0.01f,   // acc_y
    0.12f,   // acc_z
    10.43f,  // gyro_x
    125.19f, // gyro_y
    13.89f   // gyro_z
};

SensorData Motion4 = {
    // 逆时针圈圈
    -0.04f, // acc_x
    0.03f,  // acc_y
    0.13f,  // acc_z
    0.24f,  // gyro_x
    90.40f, // gyro_y
    -11.87f // gyro_z
};

// Function prototypes
void MPU6050_Init(void);

int Gesture_Recognition(void);

void MPU6050_GetData(SensorData *data);

void MPU6050_Shoushi_init(void);

void MPU6050_WriteReg(uint8_t RegAddress, uint8_t Data);

uint8_t MPU6050_ReadReg(uint8_t RegAddress);

void kalman_filter_init(kalman_filter_t *kf, float q, float r, float x_init, float p_init);

float kalman_filter_update(kalman_filter_t *kf, float measurement);

float calculateEuclideanDistance(SensorData current, SensorData reference);

int isGestureMatched(SensorData current, SensorData reference, float threshold);

void readSensorData(SensorData *data); // Function to read sensor data

// Example data points representing standard gestures (simplified for demonstration)

#define NUM_FEATURES 6             // Number of features (3 Acc + 3 Gyro)
#define NUM_SAMPLES_PER_GESTURE 40 // Number of samples to average for each gesture
#define THRESHOLD 150              // Threshold to determine if the gesture matches

// 手势识别程序
void readSensorData(SensorData *data)
{
    MPU6050_GetData(data);
}

void calculate_accel_angle(int16_t AccX, int16_t AccY, int16_t AccZ)
{
    // 计算角度
    current_angle.roll = atan2(AccX, sqrt(AccY * AccY + AccZ * AccZ)) * 180.0f / M_PI;
    current_angle.pitch = atan2(-AccY, AccZ) * 180.0f / M_PI;
}

// 从陀螺仪数据更新角度
void update_gyro_angle(float GyroX, float GyroY, float GyroZ, float dt)
{

    // 积分得到角度
    current_angle.pitch += GyroX * dt*100;
    current_angle.roll += GyroY * dt*100;
    current_angle.yaw += GyroZ * dt*100;
}

// 计算两个 n 维空间点之间的欧几里得距离
float calculateEuclideanDistance(SensorData current, SensorData reference)
{
    // 初始化平方差的总和
    float sum = pow((current.acc_x - reference.acc_x), 2) +
                pow((current.acc_y - reference.acc_y), 2) +
                pow((current.acc_z - reference.acc_z), 2) +
                pow((current.gyro_x - reference.gyro_x), 2) +
                pow((current.gyro_y - reference.gyro_y), 2) +
                pow((current.gyro_z - reference.gyro_z), 2);
    
    // 计算总和的平方根，得到欧几里得距离
    return sqrt(sum);
}

// 检查当前传感器读数是否匹配已知的手势
int isGestureMatched(SensorData current, SensorData reference, float threshold)
{
    // 当前函数未使用阈值参数
    unused(threshold);

    // 计算当前传感器数据与参考传感器数据之间的欧几里得距离
    float distance = calculateEuclideanDistance(current, reference);

    // 将距离转换为整数（根据应用场景，这一步可能不需要）
    int dis = (int)distance;

    // 返回距离的整数值
    return dis;
    // 以检查距离是否落在特定范围内，然后再返回匹配结果。
}

// 对多个传感器读数取平均值，得到手势的平均表示
void averageGestureSamples(SensorData samples[], int numSamples, SensorData *average)
{
    // 使用零初始化平均传感器数据结构
    memset(average, 0, sizeof(SensorData));

    // 汇总所有传感器读数
    for (int i = 0; i < numSamples; i++)
    {
        *average = (SensorData){
            .acc_x = average->acc_x + samples[i].acc_x,
            .acc_y = average->acc_y + samples[i].acc_y,
            .acc_z = average->acc_z + samples[i].acc_z,
            .gyro_x = average->gyro_x + samples[i].gyro_x,
            .gyro_y = average->gyro_y + samples[i].gyro_y,
            .gyro_z = average->gyro_z + samples[i].gyro_z};
    }

    // 将每个分量除以样本数量，得到平均值
    average->acc_x /= numSamples;
    average->acc_y /= numSamples;
    average->acc_z /= numSamples;
    average->gyro_x /= numSamples;
    average->gyro_y /= numSamples;
    average->gyro_z /= numSamples;
}

// 卡尔曼滤波
void kalman_filter_init(kalman_filter_t *kf, float q, float r, float x_init, float p_init)
{
    kf->Q = q;
    kf->R = r;
    kf->P = p_init;
    kf->K = 0;
    kf->X = x_init;
}

float kalman_filter_update(kalman_filter_t *kf, float measurement)
{
    float innovation = measurement - kf->X;
    kf->K = kf->P / (kf->P + kf->R);
    kf->X += kf->K * innovation;
    kf->P = (1 - kf->K) * kf->P + kf->Q;
    return kf->X;
}

void MPU6050_WriteReg(uint8_t RegAddress, uint8_t Data)
{
    i2c_data_t mpudata = {0};
    uint8_t databuff[2] = {RegAddress, Data};
    mpudata.send_buf = databuff;
    mpudata.send_len = sizeof(databuff);
    uint32_t ret = uapi_i2c_master_write(CONFIG_I2C_MASTER_BUS_ID, MPU6050_ADDRESS, &mpudata);
    if (ret != ERRCODE_SUCC)
    {
        osal_printk("i2c%d master send error %x !\r\n", CONFIG_I2C_MASTER_BUS_ID, ret);
    }
}

uint8_t MPU6050_ReadReg(uint8_t RegAddress)
{
    i2c_data_t mpudata1 = {0};
    uint8_t databuff[2] = {0};
    mpudata1.send_buf = &RegAddress;
    mpudata1.send_len = sizeof(RegAddress);
    mpudata1.receive_buf = databuff;
    mpudata1.receive_len = sizeof(databuff);
    uint32_t ret = uapi_i2c_master_writeread(CONFIG_I2C_MASTER_BUS_ID, MPU6050_ADDRESS, &mpudata1);
    if (ret != ERRCODE_SUCC)
    {
        osal_printk("i2c%d master read error %x!\r\n", CONFIG_I2C_MASTER_BUS_ID, ret);
    }
    return databuff[0];
}

void MPU6050_Init(void)
{
    /*MPU6050寄存器初始化，需要对照MPU6050手册的寄存器描述配置，此处仅配置了部分重要的寄存器*/
    MPU6050_WriteReg(MPU6050_RA_PWR_MGMT_1, 0X80); // 复位MPU6050
    osal_udelay(20000);
    MPU6050_WriteReg(MPU6050_RA_PWR_MGMT_1, 0X00);  // 唤醒MPU6050
    MPU6050_WriteReg(MPU6050_RA_INT_ENABLE, 0X00);  // 关闭所有中断
    MPU6050_WriteReg(MPU6050_RA_USER_CTRL, 0X00);   // I2C主模式关闭
    MPU6050_WriteReg(MPU6050_RA_FIFO_EN, 0X00);     // 关闭FIFO
    MPU6050_WriteReg(MPU6050_RA_INT_PIN_CFG, 0X80); // 中断的逻辑电平模式,设置为0，中断信号为高电；设置为1，中断信号为低电平时。
    // MotionInterrupt();                              // 运动中断
    MPU6050_WriteReg(MPU6050_RA_CONFIG, 0x04);       // 配置外部引脚采样和DLPF数字低通滤波器
    MPU6050_WriteReg(MPU6050_RA_ACCEL_CONFIG, 0x1C); // 加速度传感器量程和高通滤波器配置
    MPU6050_WriteReg(MPU6050_RA_INT_PIN_CFG, 0X1C);  // INT引脚低电平平时
    MPU6050_WriteReg(MPU6050_RA_INT_ENABLE, 0x40);   // 中断使能寄存器
}


void MPU6050_GetData(SensorData *data)
{
    uint8_t DataH, DataL; // 定义数据高8位和低8位的变量

    // 初始化Kalman滤波器
    kalman_filter_t kf_acc_x, kf_acc_y, kf_acc_z;
    kalman_filter_t kf_gyro_x, kf_gyro_y, kf_gyro_z;

    // 例如，Q = 0.001, R = 0.03, X = 初始估计值, P = 初始协方差估计值
    kalman_filter_init(&kf_acc_x, 0.001, 0.03, 0.0, 1.0);
    kalman_filter_init(&kf_acc_y, 0.001, 0.03, 0.0, 1.0);
    kalman_filter_init(&kf_acc_z, 0.001, 0.03, 0.0, 1.0);
    kalman_filter_init(&kf_gyro_x, 0.001, 0.03, 0.0, 1.0);
    kalman_filter_init(&kf_gyro_y, 0.001, 0.03, 0.0, 1.0);
    kalman_filter_init(&kf_gyro_z, 0.001, 0.03, 0.0, 1.0);

    // 读取加速度计和陀螺仪数据
    DataH = MPU6050_ReadReg(MPU6050_ACCEL_XOUT_H);                  // 读取加速度计X轴的高8位数据
    DataL = MPU6050_ReadReg(MPU6050_ACCEL_XOUT_L);                  // 读取加速度计X轴的低8位数据
    AccX = (DataH << 8) | DataL;                                    // 数据拼接
    data->acc_x = kalman_filter_update(&kf_acc_x, AccX / 16384.0f); // 应用Kalman滤波器并转换单位

    DataH = MPU6050_ReadReg(MPU6050_ACCEL_YOUT_H);                  // 读取加速度计Y轴的高8位数据
    DataL = MPU6050_ReadReg(MPU6050_ACCEL_YOUT_L);                  // 读取加速度计Y轴的低8位数据
    AccY = (DataH << 8) | DataL;                                    // 数据拼接
    data->acc_y = kalman_filter_update(&kf_acc_y, AccY / 16384.0f); // 应用Kalman滤波器并转换单位

    DataH = MPU6050_ReadReg(MPU6050_ACCEL_ZOUT_H);                  // 读取加速度计Z轴的高8位数据
    DataL = MPU6050_ReadReg(MPU6050_ACCEL_ZOUT_L);                  // 读取加速度计Z轴的低8位数据
    AccZ = (DataH << 8) | DataL;                                    // 数据拼接
    data->acc_z = kalman_filter_update(&kf_acc_z, AccZ / 16384.0f); // 应用Kalman滤波器并转换单位

    DataH = MPU6050_ReadReg(MPU6050_GYRO_XOUT_H);                    // 读取陀螺仪X轴的高8位数据
    DataL = MPU6050_ReadReg(MPU6050_GYRO_XOUT_L);                    // 读取陀螺仪X轴的低8位数据
    GyroX = (DataH << 8) | DataL;                                    // 数据拼接
    data->gyro_x = kalman_filter_update(&kf_gyro_x, GyroX / 131.0f); // 应用Kalman滤波器并转换单位

    DataH = MPU6050_ReadReg(MPU6050_GYRO_YOUT_H);                    // 读取陀螺仪Y轴的高8位数据
    DataL = MPU6050_ReadReg(MPU6050_GYRO_YOUT_L);                    // 读取陀螺仪Y轴的低8位数据
    GyroY = (DataH << 8) | DataL;                                    // 数据拼接
    data->gyro_y = kalman_filter_update(&kf_gyro_y, GyroY / 131.0f); // 应用Kalman滤波器并转换单位

    DataH = MPU6050_ReadReg(MPU6050_GYRO_ZOUT_H);                    // 读取陀螺仪Z轴的高8位数据
    DataL = MPU6050_ReadReg(MPU6050_GYRO_ZOUT_L);                    // 读取陀螺仪Z轴的低8位数据
    GyroZ = (DataH << 8) | DataL;                                    // 数据拼接
    data->gyro_z = kalman_filter_update(&kf_gyro_z, GyroZ / 131.0f); // 应用Kalman滤波器并转换单位

    char kalman_strings[6][20];
    snprintf(kalman_strings[0], sizeof(kalman_strings[0]), "%.2f", data->acc_x);
    snprintf(kalman_strings[1], sizeof(kalman_strings[1]), "%.2f", data->acc_y);
    snprintf(kalman_strings[2], sizeof(kalman_strings[2]), "%.2f", data->acc_z);
    snprintf(kalman_strings[3], sizeof(kalman_strings[3]), "%.2f", data->gyro_y);
    snprintf(kalman_strings[4], sizeof(kalman_strings[4]), "%.2f", data->gyro_x);
    snprintf(kalman_strings[5], sizeof(kalman_strings[5]), "%.2f", data->gyro_z);

    // 输出格式化后的字符串
    printf("MPU6050_Data=kalman_acc_x:%s,kalman_acc_y:%s,kalman_acc_z:%s,kalman_gyro_x:%s,kalman_gyro_y:%s,kalman_gyro_z:%s\r\n",
           kalman_strings[0], kalman_strings[1], kalman_strings[2],
           kalman_strings[3], kalman_strings[4], kalman_strings[5]);
}

static void app_i2c_init_pin(void)
{
    /* I2C pinmux. */
    uapi_pin_set_mode(CONFIG_I2C_SCL_MASTER_PIN, CONFIG_I2C_MASTER_PIN_MODE);
    uapi_pin_set_mode(CONFIG_I2C_SDA_MASTER_PIN, CONFIG_I2C_MASTER_PIN_MODE);
    // 设置数据线上拉
    uapi_pin_set_pull(CONFIG_I2C_SDA_MASTER_PIN, PIN_PULL_TYPE_STRONG_UP);
    uapi_pin_set_pull(CONFIG_I2C_SCL_MASTER_PIN, PIN_PULL_TYPE_STRONG_UP);
    // 设置默认为低电平
    uapi_gpio_set_val(CONFIG_I2C_SCL_MASTER_PIN, GPIO_LEVEL_HIGH);
    uapi_gpio_set_val(CONFIG_I2C_SDA_MASTER_PIN, GPIO_LEVEL_HIGH);
    printf("i2c_init_pin succ");
}

// static void gpio_callback_func(pin_t pin, uintptr_t param)
// {
//     UNUSED(pin);
//     UNUSED(param);
//     MPU6050_Shoushi();
//     // printf("动作录制.\r\n");
// }

void MPU6050_Shoushi_init(void)
{
    uint32_t baudrate = I2C_SET_BAUDRATE;
    uint8_t hscode = I2C_MASTER_ADDR;
    app_i2c_init_pin();
    errcode_t ret = uapi_i2c_master_init(CONFIG_I2C_MASTER_BUS_ID, baudrate, hscode);
    if (ret != 0)
    {
        printf("i2c init failed, ret = %0x\r\n", ret);
    }
}

int Gesture_Recognition_High(void)
{
    SensorData *motion1Samples = (SensorData *)malloc(NUM_SAMPLES_PER_GESTURE * sizeof(SensorData));
    if (motion1Samples == NULL)
    {
        printf("Memory allocation failed for motion1Samples\n");
        return -1; // 返回错误代码
    }

    for (int i = 0; i < NUM_SAMPLES_PER_GESTURE; ++i)
    {
        readSensorData(&motion1Samples[i]);
    }

    averageGestureSamples(motion1Samples, NUM_SAMPLES_PER_GESTURE, &standardMotion1);
    int flag1 = isGestureMatched(standardMotion1, Motion1, THRESHOLD);
    printf("distance1 =%d\r\n", flag1);
    int flag2 = isGestureMatched(standardMotion1, Motion2, THRESHOLD);
    printf("distance2 =%d\r\n", flag2);
    int flag3 = isGestureMatched(standardMotion1, Motion3, THRESHOLD);
    printf("distance3 =%d\r\n", flag3);
    int flag4 = isGestureMatched(standardMotion1, Motion4, THRESHOLD);
    printf("distance4 =%d\r\n", flag4);

    // 动作判断
    if (flag1 >= 155 && flag1 <= 190)
    {
        printf("Detected Motion 1\n");
        Gesture_flag = 1;
    }
    else if (flag2 >= 75 && flag2 <= 115)
    {
        printf("Detected Motion 2\n");
        Gesture_flag = 2;
    }
    else if (flag3 >= 150 && flag3 <= 185)
    {
        printf("Detected Motion 3\n");
        Gesture_flag = 3;
    }
    else if (flag4 >= 100 && flag4 <= 140)
    {
        printf("Detected Motion 4\n");
        Gesture_flag = 4;
    }

    free(motion1Samples); // 释放动态分配的内存

    return Gesture_flag;
}

int Gesture_Recognition_Low(void)
{
    char angle_strings[3][20];
    SensorData *motion1Samples = (SensorData *)malloc(NUM_SAMPLES_PER_GESTURE * sizeof(SensorData));
    if (motion1Samples == NULL)
    {
        printf("Memory allocation failed for motion1Samples\n");
        return -1; // 返回错误代码
    }

    for (size_t i = 0; i < 30; i++)
    {

        readSensorData(motion1Samples);
        // 转换加速度计数据为角度
        calculate_accel_angle(motion1Samples->acc_x, motion1Samples->acc_y, motion1Samples->acc_z);

        // 获取时间间隔
        float dt = I2C_TASK_DURATION_MS / 1000.0f;

        // 使用陀螺仪数据更新角度
        update_gyro_angle(motion1Samples->gyro_x, motion1Samples->gyro_y, motion1Samples->gyro_z, dt);

        snprintf(angle_strings[0], sizeof(angle_strings[0]), "%.2f", current_angle.pitch);
        snprintf(angle_strings[1], sizeof(angle_strings[1]), "%.2f", current_angle.roll);
        snprintf(angle_strings[2], sizeof(angle_strings[2]), "%.2f", current_angle.yaw);
        printf("Angle_Data1 = %s,Angle_Data2 = %s,Angle_Data3 = %s\r\n", angle_strings[0], angle_strings[1], angle_strings[2]);
        // 基础手势判断
        if (current_angle.pitch > 30)
        {
            shanghui++;
            if (shanghui > 5)
            {

                printf(" shang hui OK\r\n");
                shanghui = 0;
                Gesture_flag = 5;
            }
        }
        else if (current_angle.pitch < -10)
        {
            xiahui++;
            if (xiahui > 5)
            {
                printf(" xia hui OK\r\n");
                xiahui = 0;
                Gesture_flag = 6;
            }
        }
        else if (current_angle.roll < -20)
        {
            zuoxuan++;
            if (zuoxuan > 5)
            {
                printf(" zuo xuan OK\r\n");
                zuoxuan = 0;
                Gesture_flag = 7;
            }
        }
        else if (current_angle.roll > 30)
        {
            youxuan++;
            if (youxuan > 5)
            {
                printf(" you xuan OK\r\n");
                youxuan = 0;
                Gesture_flag = 8;
            }
        }
        else
        {
            printf("No known gesture detected\n");
            Gesture_flag = 0;
        }
    }

    free(motion1Samples); // 释放动态分配的内存

    return Gesture_flag;
}