#include "pinctrl.h"
#include "uart.h"
#include "watchdog.h"
#include "soc_osal.h"
#include "app_init.h"
#include <math.h>

#include "gpio.h"
#include "osal_debug.h"
#include "cmsis_os2.h"
#include "../inc/infrared.h"

#define UART_TRANSFER_SIZE 800     // 传输缓冲区大小
#define UART_TRANSFER_BUFF_SIZE 16 // 传输缓冲区大小

#define UART_BAUDRATE 115200
#define UART_DATA_BITS 3
#define UART_STOP_BITS 1
#define UART_PARITY_BIT 0

#define CONFIG_UART_BUS_ID 2
#define CONFIG_UART_TXD_PIN 8
#define CONFIG_UART_RXD_PIN 7
#define CONFIG_UART_PIN_MODE 2

uint8_t buf[32], len;

uint8_t getSum(uint8_t *data, uint8_t len)
{
    uint8_t i, sum = 0;
    for (i = 0; i < len; i++)
    {
        sum += data[i];
    }
    return sum;
}

uint8_t IrStudy(uint8_t *data, uint8_t group)
{
    uint8_t *offset = data, cs;
    // 帧头
    *offset++ = 0x68;
    // 帧长度
    *offset++ = 0x08;
    *offset++ = 0x00;
    // 模块地址
    *offset++ = 0xff;
    // 功能码 - 学习
    *offset++ = 0x10;
    // 内码索引号，代表第几组
    *offset++ = group;
    cs = getSum(&data[3], offset - data - 3);
    *offset++ = cs;
    *offset++ = 0x16;
    return offset - data;
}

uint8_t IrSend(uint8_t *data, uint8_t group)
{
    uint8_t *offset = data, cs;
    // 帧头
    *offset++ = 0x68;
    // 帧长度
    *offset++ = 0x08;
    *offset++ = 0x00;
    // 模块地址
    *offset++ = 0xff;
    // 功能码 - 发送
    *offset++ = 0x12;
    // 内码索引号，代表第几组
    *offset++ = group;
    cs = getSum(&data[3], offset - data - 3);
    *offset++ = cs;
    *offset++ = 0x16;
    return offset - data;
}

// 接收数据直接缓冲区
static uint8_t g_app_uart_rx_buff[UART_TRANSFER_BUFF_SIZE] = {0};

static uart_buffer_config_t g_app_uart_buffer_config = {
    .rx_buffer = g_app_uart_rx_buff,
    .rx_buffer_size = UART_TRANSFER_BUFF_SIZE};

void infrared_init(void)
{

    uapi_pin_set_mode(CONFIG_UART_TXD_PIN, CONFIG_UART_PIN_MODE);
    uapi_pin_set_mode(CONFIG_UART_RXD_PIN, CONFIG_UART_PIN_MODE);
    osal_printk("app_uart_init_pin suc\r\n");

    uart_attr_t attr = {
        .baud_rate = UART_BAUDRATE,
        .data_bits = UART_DATA_BITS,
        .stop_bits = UART_STOP_BITS,
        .parity = UART_PARITY_BIT};

    uart_pin_config_t pin_config = {
        .tx_pin = CONFIG_UART_TXD_PIN,
        .rx_pin = CONFIG_UART_RXD_PIN,
        .cts_pin = PIN_NONE,
        .rts_pin = PIN_NONE};

    uapi_uart_init(CONFIG_UART_BUS_ID, &pin_config, &attr, NULL, &g_app_uart_buffer_config);
    osal_printk("红外串口初始化成功\r\n");
}

/*内码学习组
1：开
2：关
3 扇叶上下
4 扇叶左右
5 制冷 24
6 制热
7 风速1档
8 风速3档
9 温度升高
10 温度下降
*/

void send_ac_power_on(void)
{
    printf("准备发送空调开机信号\n");

    len = IrSend(buf, 0);
    printf("IrSend 返回长度: %d\n", len);

    int write_result = uapi_uart_write(CONFIG_UART_BUS_ID, buf, len, 1000);
    printf("uapi_uart_write 返回结果: %d\n", write_result);

    printf("空调开机信号发送完成\n");
    PRINT("空调打开\n", 0);
}

void send_ac_power_off(void)
{
    len = IrSend(buf, 1);
    PRINT("空调关闭\n", 1);
    uapi_uart_write(CONFIG_UART_BUS_ID, buf, len, 1000);

}

void send_ac_fan_swing_up_down(void)
{
    len = IrSend(buf, 2);
    PRINT("风扇上下摆动\n", 2);
    uapi_uart_write(CONFIG_UART_BUS_ID, buf, len, 1000);

}

void send_ac_fan_swing_left_right(void)
{
    len = IrSend(buf, 3);
    PRINT("风扇左右摆动\n", 3);
    uapi_uart_write(CONFIG_UART_BUS_ID, buf, len, 1000);

}

void send_ac_cool(void)
{
    len = IrSend(buf, 4);
    PRINT("空调制冷\n", 4);
    uapi_uart_write(CONFIG_UART_BUS_ID, buf, len, 1000);

}

void send_ac_heat(void)
{
    len = IrSend(buf, 5);
    PRINT("空调制热\n", 5);
    uapi_uart_write(CONFIG_UART_BUS_ID, buf, len, 1000);

}

void send_ac_fan_speed_1(void)
{
    len = IrSend(buf, 6);
    PRINT("风扇速度1\n", 6);
    uapi_uart_write(CONFIG_UART_BUS_ID, buf, len, 1000);

}

void send_ac_fan_speed_3(void)
{
    len = IrSend(buf, 7);
    PRINT("风扇速度3\n", 7);
    uapi_uart_write(CONFIG_UART_BUS_ID, buf, len, 1000);

}

void send_ac_temp_up(void)
{
    len = IrSend(buf, 8);
    PRINT("温度升高\n", 8);
    uapi_uart_write(CONFIG_UART_BUS_ID, buf, len, 1000);
;
}

void send_ac_temp_down(void)
{
    len = IrSend(buf, 9);
    PRINT("温度降低\n", 9);
    uapi_uart_write(CONFIG_UART_BUS_ID, buf, len, 1000);

}