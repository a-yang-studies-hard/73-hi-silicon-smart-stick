#ifndef INFRARED_H
#define INFRARED_H

#include <stdint.h>

// 初始化红外模块
void infrared_init(void);

// 发送空调打开命令
void send_ac_power_on(void);

// 发送空调关闭命令
void send_ac_power_off(void);

// 发送扇叶上下摆动命令
void send_ac_fan_swing_up_down(void);

// 发送扇叶左右摆动命令
void send_ac_fan_swing_left_right(void);

// 发送制冷命令
void send_ac_cool(void);

// 发送制热命令
void send_ac_heat(void);

// 发送风速1档命令
void send_ac_fan_speed_1(void);

// 发送风速3档命令
void send_ac_fan_speed_3(void);

// 发送温度升高命令
void send_ac_temp_up(void);

// 发送温度降低命令
void send_ac_temp_down(void);

#endif // INFRARED_H
